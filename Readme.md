
# MARE system Präsentationen und Vorträge

Vorträge, Folien, Bechmarks etc sind in den einzelnen
Unterordern zu finden 

# Liste: [Link](https://bitbucket.org/maresystem/vortr-ge-pr-sentationen/src)


## Naxsi - Owasp Hamburg 2013

- Präsentation vor dem Hamburger Stammtisch des OWASP German Chapter und der
Hamburger Abteilung der GUUG
- Naxsi als WAF
  - Intro
  - Aufbau
  - Arbeitsweise
  - Doxi-Tools & Rules
  - Reporting



## Nginx als Frontend - Gateway - Cebit 2013  

- Präsentation im Rahmen des Open-Source-Forums zusammen 
mit Thibault "bui" Koechlin von NBS system, Entwickler der Naxsi - Firewall
- Nginx als Frontend-Gateway
  - Reverse Proxy
  - Loadbalancer
  - Static Server
  - WebCache
  - WAF (bui)
  

