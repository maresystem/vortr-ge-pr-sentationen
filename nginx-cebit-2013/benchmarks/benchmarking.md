# benchmarking nginx as static server / proxy-cache


## setup

- apache + mod_python + custom cms as application-server, different machine 
- nginx with static content + proxy_cache (test.example.com:80)
- cache -> /tmp/nginx as 100 MB tmpfs 
- nginx + naxsi + full doxi-rules 

- testtool: ab ( using abmeter, a wrapper for ab that  generates better csv-output)

- every request
    - 100.000 times 
    - 100,200....1000 parallel requests
    - 


## server 

- 4 Cores
- 2 GB RAM
- debian-based virtual machine (kvm)
- no other services running
- only vm @ virtual master
- limits.conf has be tweaked


## nginx.conf



    
    
    user  nobody;
    worker_processes  4;
    worker_rlimit_nofile 100000;
    error_log  /var/log/nginx/error.log;
    
    
    
    
    events {
        worker_connections 10000;
    }
    
    
    http {
        include       mime.types;
        default_type  application/octet-stream;
    
        server_names_hash_bucket_size 64;
    
    
        #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
        #                  '$status $body_bytes_sent "$http_referer" '
        #                  '"$http_user_agent" "$http_x_forwarded_for"';
    
        access_log  off;
    
        sendfile        on;
        tcp_nopush     on;
    
        #keepalive_timeout  0;
        keepalive_timeout  15;
        server_tokens off;
    
    
        gzip  off;
    
            # doxi-rules
    
    
        proxy_cache_path    /tmp/nginx    levels=1:2  keys_zone=default:50m inactive=10m max_size=50m;
    
    
        include /etc/nginx/sites/*.conf;
    
    
    
    }


## sites.conf

    server {
        listen       192.168.122.40:80;
        server_name  www.example.com test.example.com 192.168.122.40;

        root /srv/data/www/homepage;

        #charset koi8-r;

        access_log  off;


        expires off;
        gzip off;

        proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
        proxy_set_header        X-Real-IP       $remote_addr;
        proxy_set_header    Host    $host;

        proxy_cache             default    ;
        proxy_cache_key         default$request_uri;
        proxy_cache_valid       200 301 302 10m;
        proxy_cache_valid       404 1m; 
        proxy_cache_valid       any 1m;
        proxy_cache_use_stale   error timeout invalid_header updating; 


        try_files $uri $uri/ @pruxy; 




        location / {
    
            proxy_pass http://sites.example.com;
    
    
    
                # doxi
                # loading naxi in learning-mode
                #include    /etc/nginx/doxi-rules/learning-mode.rules;
                # OR loading naxi in active-mode
                include    /etc/nginx/doxi-rules/active-mode.rules;
                # include the subset of loadable rules
    
    
        }


        location @pruxy  {
            proxy_pass http://sites.example.com;
	    
                            # doxi
            # loading naxi in learning-mode
            #include    /etc/nginx/doxi-rules/learning-mode.rules;
            # OR loading naxi in active-mode
            include    /etc/nginx/doxi-rules/active-mode.rules;
            # include the subset of loadable rules

        }



        location /RequestDenied {
            proxy_pass http://127.0.0.1:4242;
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

    }


